from urllib import request
from urllib import error
import os
from googlesearch import search
import re

TAG_RE = re.compile(r'<[^>]+>')


def fetch_page(siteURL):
    ## create a variable which will hold our desired web page as a string
    site = siteURL
    ## create the approprriate headers for our http request so that we wont run
    ## into any 403 forbidden errors.
    hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}

    ## Perform a HTTP request by passing in our desired URL and setting our headers to equal
    ## the headers that we've defined above.
    try:
        page = request.urlopen(site)
        ## lastly we will want to read the response which was generated by opening
        ## the url and store it under content
        content = page.read()
    except error.HTTPError as e:
         content = None

    return content


def remove_tags(text):
    return TAG_RE.sub('', text)


def keyword_counts(company_name, keyword_dict):
    word_counts = {}
    # keep track of visited web pages to avoid double counting words
    searched_urls = []
    base_sitename = ""

    try:
        company_page = next(search("\"" + company_name.lower() + "\"", num=1))
        print(company_page)
        searched_urls.append(company_page)

        # A previous approach to finding the base site name for a company #
        # if company_page.find("www.") > 0:
        #     base_sitename = company_page[company_page.find("www.")+4:]
        # else:
        #     base_sitename = company_page[company_page.find("https://") + 8:]
        #
        # base_sitename = base_sitename[:base_sitename.find(".")]
        base_sitename = company_page[:company_page.rfind(".html")]
        base_sitename = base_sitename[:base_sitename.rfind(".")]
        base_sitename = base_sitename[base_sitename.rfind(".")+1:]
    except Exception as e:
        print(e)
        return word_counts

    for key in keyword_dict:
        word_counts[key] = 0

    for key in keyword_dict:
        try:
            company_page = next(search(" ".join(["\""+company_name.lower()+"\"", key]), num=1))
        except:
            continue
        print(company_page)
        if company_page.find(base_sitename) < 0:
            continue
        if company_page not in searched_urls:
            searched_urls.append(company_page)
            page_content = fetch_page(company_page)
            data = str(page_content).split(" ")
            text_content = [remove_tags(line) for line in data]
            # print(text_content)
            for key in keyword_dict:
                for word in text_content:
                    if word.lower() in keyword_dict[key]:
                        word_counts[key] += 1

    return word_counts


# print(keyword_counts("lego", {"recycle": ["recycle", "recycled", "recycling", "recycles", "recyclable", "recyclables", "waste", "reuse", "reusable"],
#                                     "compost": ["compost", "compostable", "compostables", "composted", "composting", "composts"],
#                                     "sustainable": ["sustainable", "sustainability"],
#                                     "renewable": ["renewable", "renewability"], "environmental": ["environmental", "environment"],
#                                     "carbon footprint": ["carbon", "emissions", "footprint"]}))