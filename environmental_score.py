from googlesearch import search

import base64
import requests
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium import webdriver


def get_driver():
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) ' \
                 'Chrome/80.0.3987.132 Safari/537.36'
    options = Options()
    options.add_argument("--no-sandbox")
    options.add_argument("--headless")
    options.add_argument(f'user-agent={user_agent}')
    options.add_argument("--disable-web-security")
    options.add_argument("--allow-running-insecure-content")
    options.add_argument("--allow-cross-origin-auth-prompt")
    new_driver = webdriver.Chrome('C:\\Users\\Max\\Downloads\\chromedriver_win32\\chromedriver.exe', options=options)
    return new_driver


def company_score(company_name):

    driver = get_driver()
    search_url = next(search(" ".join(["\"search sustainability ratings\" csrhub", company_name.lower()]), num=1))
    print(search_url)
    driver.get(search_url)

    # first_search_result = driver.find_elements_by_xpath('//*/div[@class="rc"]/div/a')[0]
    # try:
    #     first_search_result.click()
    # except:
    #     return -1

    score_element = driver.find_element_by_xpath('//*/td[@style]')
    score = int(score_element.text)
    driver.close()
    return score
